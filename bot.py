import tweepy
import config
#comment for example
auth = tweepy.OAuth1UserHandler(config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret
)

api = tweepy.API(auth) 

#prints the text of the tweet using hastag designated in stream.filter(track=[])
class LogTweets(tweepy.Stream):
        def on_status(self, status):
                date = status.created_at
                username = status.user.screen_name
                try:
                        tweet =status.extended_tweet["full_text"]
                except AttributeError:
                        tweet =status.text

                print("**Tweet info**")
                print(f"Date: {date}")
                print(f"Username: {username}")
                print(f"Tweet: {tweet}")
                print("*********")
                print("********* \n")
              

if __name__ == "__main__":         
        #creates instance of LogTweets with authentication
        stream = LogTweets(config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret)


        #hashtags as str in array will be watched live on twitter. 
        hashtag = input("What hashtag would you like to look for on Twitter?")
        print("Looking for Hashtags...")
        stream.filter(track=[f"#{hashtag}"])

